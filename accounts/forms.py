from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


# Create a sign-up form with the following fields:
# username that has a max length of 150 characters
# password that has a max length of 150 characters and uses a PasswordInput
# password_confirmation that has a max length of 150 characters and uses


class SignupForm(forms.Form):
    username = forms.Charfield(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )


# see similar format below as reference

# class ExpenseCategory(models.Model):
# name = models.CharField(max_length=50)
# owner = models.ForeignKey(
# User, related_name="categories", on_delete=models.CASCADE
