from django.shortcuts import render, redirect
from accounts.forms import LoginForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# follow above method, lot less steps def user_login(request)
def user_logout(request):
    logout(request)
    return redirect("login")


# see reference below create views

# def create_model_name(request):
# if request.method == "POST":
# form = ModelForm(request.POST)
# if form.is_valid():
# To redirect to the detail view of the model, use this:
# model_instance = form.save()
# return redirect("detail_url", id=model_instance.id)

# To add something to the model, like setting a user,
# use something like this:
#
# model_instance = form.save(commit=False)
# model_instance.user = request.user
# model_instance.save()
# return redirect("detail_url", id=model_instance.id)
# else:
# form = ModelForm()

# context = {
# "form": form
# }

# return render(request, "model_names/create.html", context)
